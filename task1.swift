import Swift

let firstName = "Philipp"
let secondName = "Kalyubin"
let age = 21
let university = "ETU LETI"

print("Hello. My name is \(firstName) \(secondName). I am \(age) years old. I study at \(university).")